package main

import (
  "fmt"
  "math/rand"
  "time"
)

func main() {
  // a unique seed value
  rand.Seed(time.Now().UnixNano())
  // boolean value
  isHeistOn := true
  // created a random number
  eludedGuards := rand.Intn(100)
  
  if eludedGuards <= 50 {
    fmt.Println("We made it past the guards!")
  } else {
    isHeistOn = false
    fmt.Println("Make a better plan next time?")
  }
  // give a random number between 1 and 99
  openedVault := rand.Intn(100)
  fmt.Println("Heist status is...", isHeistOn)
  if openedVault >= 70 && isHeistOn == true {
    fmt.Println("Grab and GO!")
  } else if isHeistOn == true {
    isHeistOn = false
    fmt.Println("The vault can't be opened!")
  }
  // give a random number between 0 and 5
  leftSafely := rand.Intn(5)
  if isHeistOn {
    switch leftSafely {
      case 0:
        isHeistOn = false
        fmt.Println("We dropped the money!")
      case 1:
        isHeistOn = false
        fmt.Println("Turns out vault doors don't open from the inside..")
      case 2:
        isHeistOn = false
        fmt.Println("We forgot the tools!")
      case 3:
        isHeistOn = false
        fmt.Println("We tripped and fell over!")
      default:
        fmt.Println("Heist success! We got away!")
    }
  }
  if isHeistOn {
    amtStolen := 10000 + rand.Intn(1000000)
    fmt.Println("We got away with $", amtStolen)
  }
}
