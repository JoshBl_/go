// paint calculator
package main

//import packages
import (
	"fmt"
	"log"
)

// function declared with parameters, returns float64 as a pointer and error
func paintCalculator(width float64, height float64) (*float64, error) {
	area := width * height
	if width < 0 {
		return &area, fmt.Errorf("A width of %0.2f is invalid", width)
	}
	if height < 0 {
		return &area, fmt.Errorf("A height of %0.2f is invalid", height)
	}
	// remember to return both values - the float64 and the error
	area = area / 10.0
	// return the address of area and nil for error
	return &area, nil
}

func main() {
	// new variables declared
	var total float64
	// this value is a pointer
	var amount *float64
	amount, err := paintCalculator(4.2, 3.0)
	fmt.Println("Wall 1:")
	if err != nil {
		log.Fatal(err)
	} else {
		// access the value at the address
		fmt.Printf("%0.2f litres needed\n", *amount)
		// adding the amount variable to total
		// access the value at the address
		total += *amount
	}

	amount, err = paintCalculator(5.2, 3.5)
	fmt.Println("Wall 2:")
	if err != nil {
		log.Fatal(err)
	} else {
		// access the value at the address
		fmt.Printf("%0.2f litres needed\n", *amount)
		// adding the amount variable to total
		// access the value at the address
		total += *amount
	}
	fmt.Printf("Total: %0.2f litres\n", total)
}
