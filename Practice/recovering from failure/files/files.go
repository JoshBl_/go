package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
)

func reportPanic() {
	p := recover() // call recover and store the return value
	if p == nil {  // if recover returned nil, do nothing
		return
	}
	err, ok := p.(error) // get the underlying error value
	if ok {
		fmt.Println(err) // print the error
	} else {
		panic(p) // if the panic value isn't an error, resume panicking with the same value
	}
}

// for recursive functions, it's better to use panic insetead of returning an error
// all the recursive calls exit
// a recursive function that takes the path to scan
func scanDirectory(path string) {
	fmt.Println(path)                  // printing the current directory
	files, err := ioutil.ReadDir(path) //gets a slice with the directory's contents
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		filePath := filepath.Join(path, file.Name()) // join the directory path and filename with a slash
		if file.IsDir() {                            // if this is a subdirectory
			scanDirectory(filePath) // recursively call scanDirectory function, with the subdirectory's path
		} else {
			fmt.Println(filePath) // if this is a file, print the path
		}
	}
}

func main() {
	defer reportPanic()                                                        // before calling code that could panic - defer a call to the reportPanic function
	scanDirectory("C:\\Users\\joshua.blewitt\\Downloads\\Coding Practice\\Go") // calling the directory - can be replaced
}
