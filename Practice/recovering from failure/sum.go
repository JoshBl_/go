package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

// OpenFile function, takes a string and returns a pointer to OS File and an error
func OpenFile(fileName string) (*os.File, error) {
	fmt.Println("Opening", fileName)
	return os.Open(fileName) // opens the file and returns a pointer to it, along with any errors
}

// CloseFile function, takes an argument of the file as a pointer and closes the file
func CloseFile(file *os.File) {
	fmt.Println("Closing file")
	file.Close()
}

// GetFloats function, takes the fileName as an argument, returns a float64 array and an error
func GetFloats(fileName string) ([]float64, error) {
	var numbers []float64
	file, err := OpenFile(fileName)
	if err != nil {
		return nil, err
	}
	defer CloseFile(file) // this will not run until GetFloats exits, even if there is an error
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		number, err := strconv.ParseFloat(scanner.Text(), 64)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, number)
	}
	if scanner.Err() != nil {
		return nil, scanner.Err()
	}
	return numbers, nil
}

func main() {
	numbers, err := GetFloats(os.Args[1]) // using the first command-line argument as a filename
	if err != nil {
		log.Fatal(err)
	}

	// adding up the numbers in the slice
	var sum float64 = 0
	for _, number := range numbers {
		sum += number
	}

	// print the total
	fmt.Printf("Sum: %0.2f\n", sum)
}
