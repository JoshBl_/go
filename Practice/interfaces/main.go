package main

import (
	"fmt"
	"gadget"
	"log"
)

// function takes a TaplePlayer value and a songs slice
func playList(device Player, songs []string) {
	for _, song := range songs { // iterate through each song
		device.Play(song)
	}
	device.Stop() // stop the player when we are done
}

// Player interface
type Player interface {
	Play(string) // Play method with a string argument
	Stop()       // stop method
}

// TryOut function - uses type assertion
func TryOut(player Player) {
	player.Play("Test track")
	player.Stop()
	// ok is a second return value to the variable
	recorder, ok := player.(gadget.TapeRecorder) // type assertion to get a TapeRecorder value and store the TapeRecorder value
	if ok {                                      // this is only called if the original value was a TapeRecorder
		recorder.Record() // calling the method that's only defined on the concrete type
	} else {
		fmt.Println("Player was not a TaperRecorder")
	}
}

// OverheatError type with an underlying type of float64
type OverheatError float64

// method that satisfies the error interface
func (o OverheatError) Error() string {
	return fmt.Sprintf("Overheating by %0.2f degrees!", o) // use the temperature in the error message
}

func checkTemperature(actual float64, safe float64) error { //function returns an ordinary error value
	excess := actual - safe
	if excess > 0 {
		return OverheatError(excess) // call the OverheatError with the excess
	}
	// if excess is equal to 0
	return nil
}

// STRINGER interface

// Stringer - interface with a string
type Stringer interface {
	String() string
}

// CoffeePot type
type CoffeePot string

// this method satifies the stringer
func (c CoffeePot) String() string {
	return string(c) + " coffee pot"
}

func main() {
	//mixTape := []string{"Jessie's Girl", "Whip it", "9 to 5"} // slice of song titles
	//var player Player = gadget.TapePlayer{}                   // holds any player
	//playList(player, mixTape)                                 // plays the songs using the TapePlayer
	//player = gadget.TapeRecorder{}
	//playList(player, mixTape)
	TryOut(gadget.TapeRecorder{})
	TryOut(gadget.TapePlayer{}) // the concrete type was TapePlayer, not TapeRecorder!

	coffeePot := CoffeePot("LuxBrew")
	fmt.Println(coffeePot.String())

	var err error = checkTemperature(121.379, 100.0)
	if err != nil {
		log.Fatal(err)
	}
}
