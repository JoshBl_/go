package main

import (
	"fmt"
	"mypkg"
)

func main() {
	var value mypkg.MyInterface // declaring a variable using the interface type
	value = mypkg.MyType(5)     // values of myType satisfy myInvterface so we can assign this value to a variable with a type of interface
	// calling methods
	value.MethodWithoutParameters()
	value.MethodWithParameters(127.3)
	fmt.Println(value.MethodWithReturnValue())
}
