package mypkg

import "fmt"

// MyInterface - declaring an interface type
type MyInterface interface {
	// a type satisfies this interface if it has these methods
	MethodWithoutParameters()
	MethodWithParameters(float64)
	MethodWithReturnValue() string
}

// MyType a type that will satisfy the interface
type MyType int

// MethodWithoutParameters - prints a line
func (m MyType) MethodWithoutParameters() {
	fmt.Println("MethodWithoutParameters called")
}

// MethodWithParameters - prints a line with the passed in argument
func (m MyType) MethodWithParameters(f float64) {
	fmt.Println("MethodWithParameters called with", f)
}

// MethodWithReturnValue returns a message
func (m MyType) MethodWithReturnValue() string {
	return "Hi from MethodWithReturnValue"
}

// MethodNotInInterface - prints a statement
func (m MyType) MethodNotInInterface() {
	fmt.Println("MethodNotInInterface called")
}
