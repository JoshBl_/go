package main

import "fmt"

// Whistle type (string)
type Whistle string

// MakeSound method for the Whistle type
func (w Whistle) MakeSound() {
	fmt.Println("Tweet!")
}

// Horn type (string)
type Horn string

// MakeSound method for the Horn type
func (h Horn) MakeSound() {
	fmt.Println("Honk!")
}

// Robot type (string)
type Robot string

// MakeSound method - this satisfies the NoiseMaker interface
func (r Robot) MakeSound() {
	fmt.Println("Beep Boop")
}

// Walk method - not part of the NoiseMaker interface
func (r Robot) Walk() {
	fmt.Println("Powering legs")
}

// REMEMBER - the interface declaration specifies the methods that a type is required to have in order to satisfy the interface

// NoiseMaker interface
type NoiseMaker interface {
	MakeSound()
}

// now we can pass any value from a type with a MakeSound method to play
func play(n NoiseMaker) {
	n.MakeSound()
	// this is not okay - it's not part of the NoiseMaker interface!
	// n.Walk()
}

func main() {
	// NoiseMaker variable
	var toy NoiseMaker
	// assign a value of a type that satisifes the NoiseMaker interface to the variable
	toy = Whistle("Toyco Canary")
	toy.MakeSound()
	// assign a value of a type that satisifes the NoiseMaker interface to the variable
	toy = Horn("Toyco Blaster")
	toy.MakeSound()

	// improved way
	play(Whistle("Toyco Canary"))
	play(Horn("Toyco Blaster"))

	// TYPE ASSERTIONS
	// define a new variable with an interface type
	var noiseMaker NoiseMaker = Robot("Botco Ambler") // assign a value of a type that satisfies the interface
	noiseMaker.MakeSound()                            // calling a method that's part of the interface
	var robot Robot = noiseMaker.(Robot)              // Convert back to the concrete type using a type assertion
	robot.Walk()                                      // Call a method that's defined on the concrete type but not the interface
}
