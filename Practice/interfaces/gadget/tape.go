package gadget

import (
	"fmt"
)

// TapePlayer - struct with Batteries field, exported
type TapePlayer struct {
	Batteries string
}

// Play method for TapePlayer - requires a string argument
func (t TapePlayer) Play(song string) {
	fmt.Println("Playing", song)
}

// Stop method for TapePlayer - no arguments required
func (t TapePlayer) Stop() {
	fmt.Println("Stopped!")
}

// TapeRecorder - struct with Microphones field, exported
type TapeRecorder struct {
	Microphones int
}

// Play method for TapeRecorder - requires a string argument
func (t TapeRecorder) Play(song string) {
	fmt.Println("Playing", song)
}

// Record method for TapeRecorder - no arguments required
func (t TapeRecorder) Record() {
	fmt.Println("Recording")
}

// Stop method for TapeRecorder - no arguments required
func (t TapeRecorder) Stop() {
	fmt.Println("Stopped!")
}
