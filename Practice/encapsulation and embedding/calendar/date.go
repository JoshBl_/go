package calendar

import (
	"errors"
	"unicode/utf8"
)

// Date - defining a new struct type
type Date struct { // the fields of the type are unexpord you can't call these directly, they are protected!
	year  int
	month int
	day   int
}

// Event - new struct type
type Event struct {
	title string
	Date  // embedded using a
}

// SetYear - setter method for the year field
func (d *Date) SetYear(year int) error { // has a pointer to the receiver - so the original value can be updated
	if year < 1 {
		return errors.New("invalid year") // if entry is invalid print message
	}
	d.year = year // automatically gets value at pointer
	return nil    // if valid return nil
}

// SetMonth - setter method for the month field
func (d *Date) SetMonth(month int) error {
	if month < 1 || month > 12 {
		return errors.New("invalid month")
	}
	d.month = month
	return nil
}

//SetDay - setter method for the day field
func (d *Date) SetDay(day int) error {
	if day < 1 || day > 31 {
		return errors.New("invalid day")
	}
	d.day = day
	return nil
}

// SetTitle - set the event title
func (e *Event) SetTitle(title string) error {
	if utf8.RuneCountInString(title) > 30 {
		return errors.New("invalid title")
	}
	e.title = title
	return nil
}

// Year - getter for the year field
func (d *Date) Year() int {
	return d.year
}

// Month - getter for the year field
func (d *Date) Month() int {
	return d.month
}

// Day - getter for the day field
func (d *Date) Day() int {
	return d.day
}

// Title - getter for the event title
func (e *Event) Title() string {
	return e.title
}
