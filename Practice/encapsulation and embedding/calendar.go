package main

import (
	"calendar"
	"fmt"
	"log"
)

func main() {
	// struct literal
	date := calendar.Date{} // creating a date

	// calling the setter methods

	err := date.SetYear(2020)

	if err != nil {
		log.Fatal(err)
	}

	err = date.SetMonth(1)

	if err != nil {
		log.Fatal(err)
	}

	err = date.SetDay(31)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(date) // print the date variable

	//calling the getter methods

	fmt.Println("The year set is:", date.Year())

	fmt.Println("The month set is:", date.Month())

	fmt.Println("The day set is:", date.Day())

	//creating a new event
	event := calendar.Event{}
	err = event.SetTitle("Birthday")
	err = event.SetYear(2020)
	err = event.SetMonth(10)
	err = event.SetDay(20)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("The event title is:", event.Title())

	fmt.Println("The year set is:", event.Year())

	fmt.Println("The month set is:", event.Month())

	fmt.Println("The day set is:", event.Day())

}
