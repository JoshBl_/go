package main

import (
	"fmt"
)

// it's generally better to use pointers when passing to a function rather than the struct itself
// functions need to make room for the original struct AND the copy

// defined new type called part
type part struct { // the underlying type for part will be a struct with fields
	description string
	count       int
}

// defined new type called part
type car struct {
	name     string
	topSpeed float64
}

type subscriber struct {
	name   string
	rate   float64
	active bool
}

func main() {
	// a variable named myStruct
	var myStruct struct { // this can hold multiple fields
		number float64
		word   string
		toggle bool
	}

	fmt.Printf("%#v\n", myStruct) // this will print out the struct as it appears in Go code

	// access the struct fields using the dot operator
	myStruct.number = 50.5
	myStruct.word = "Test"
	myStruct.toggle = true
	fmt.Println(myStruct.number)
	fmt.Println(myStruct.word)
	fmt.Println(myStruct.toggle)

	fmt.Println("")

	// declaring a variable of type car
	//var tesla car
	// acessing the struct fields
	//tesla.name = "Tesla Model 3"
	//tesla.topSpeed = 220
	// calling function
	//showCarInfo(tesla)

	tesla := newCarCreation("Tesla Model 3", 220)
	showCarInfo(tesla)

	fmt.Println("")

	// declaring a variable of type part
	//var bolts part
	// accessing the struct fields
	//bolts.description = "Hex bolts"
	//bolts.count = 24
	//fmt.Println("Description:", bolts.description)
	//fmt.Println("Count:", bolts.count)

	//fmt.Println("")

	// call the function, use a short hand variable declaration to store the returned part
	newBolt := newPartCreation("New bolts")
	showPartInfo(newBolt)

	fmt.Println()

	joshua := setAccount("Joshua") // this is a struct pointer
	showSubscriberInfo(joshua)
	applyDiscount(joshua)
	showSubscriberInfo(joshua) // showing the updated variable after changing a field

	// struct literal
	vauxhall := car{name: "Vauxhall Viva", topSpeed: 165}
	fmt.Println(vauxhall.name)
	fmt.Println(vauxhall.topSpeed)

}

func newCarCreation(name string, speed float64) *car {
	var c car // new car value
	c.name = name
	c.topSpeed = speed
	return &c
}

// using a function to access the fields of a struct
func showCarInfo(c *car) {
	fmt.Println("Description:", c.name)
	fmt.Println("Top Speed:", c.topSpeed)
}

// function to create a new value of a type
func newPartCreation(description string) *part { // return one value, a type of part
	var p part //new part value
	p.description = description
	p.count = 100
	return &p
}

func showPartInfo(p *part) {
	fmt.Println("Description:", p.description)
	fmt.Println("Count:", p.count)
}

func setAccount(details string) *subscriber {
	var account subscriber
	account.name = details
	account.rate = 10.99
	account.active = true
	return &account
}

func showSubscriberInfo(s *subscriber) {
	fmt.Println("Details:\nName:", s.name, "\nRate:", s.rate, "\nActive status:", s.active)
}

// takes a pointer to a struct - not the struct itself
func applyDiscount(s *subscriber) {
	s.rate = 4.99 // update the field in the struct
}
