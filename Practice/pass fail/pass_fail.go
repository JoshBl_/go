// pass_fail reports whether a grade is passing or failing
package main

// import packages needed
import (
	"fmt"
	"log"
)

func main() {
	fmt.Print("Enter a grade: ")

	grade, err := getFloat()
	if err != nil {
		log.Fatal(err)
	}

	var status string
	// don't name a variable error, it would shadow the name of a type called error
	if err != nil {
		log.Fatal(err)
	}
	if grade >= 60 {
		status = "passing"
	} else {
		status = "failing"
	}
	fmt.Println("A grade of", grade, "is", status)
}
