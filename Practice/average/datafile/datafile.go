package datafile

import (
	"bufio"
	"os"
	"strconv"
)

// GetFloats reads a float64 from each line of a file
// function returns an array of numbers and any errors
func GetFloats(filename string) ([]float64, error) {
	var numbers []float64 //this is the array to be returned

	// open the data file for reading
	file, err := os.Open(filename)

	// if there was an error, log it and exit
	if err != nil {
		return nil, err // return nil instead of the slice - it would be nil at this point
	}

	scanner := bufio.NewScanner(file) // new scanner for the file
	// reads a line from the file
	for scanner.Scan() {
		number, err := strconv.ParseFloat(scanner.Text(), 64) // convert the file line string to a float64
		if err != nil {
			return nil, err // return nil instead of the slice
		}
		numbers = append(numbers, number)
	}

	// close the file to free resources
	err = file.Close()

	// if there are any errors with closing the file, log it and exit
	if err != nil {
		return nil, err // return nil
	}

	// if there are any errors with the scanner, log it and exit
	if scanner.Err() != nil {
		return nil, err // return nil
	}

	return numbers, nil // no errors at this point, so return the array and nil error

}
