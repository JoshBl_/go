// practicing variadic functions
package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Variadic functions")
	// prints an array!
	severalInts(1, 2, 3, 4, 5)
	severalStrings() // if there are no arguments, empty slice is received
	severalStrings("a", "b", "c", "d", "e", "f")
	mix(1, true, "hello", "world", "test")
	fmt.Println(maximum(55.3, 41.2, 87.6, 21.8))
	fmt.Println(maximum(17.5, 24.2, 75.4, 83.6, 39.2, 43.1, 99.9, 9.1))
	fmt.Println(inRange(10, 50, 9, 3, 87, 34, 49, 21, 63, 78, 84, 26, 40, 38))
}

// variadic function - defined by three '.', or known as an Ellipsis!
func severalInts(numbers ...int) {
	fmt.Println(numbers)
}

// another variadic function, this one uses strings
func severalStrings(strings ...string) {
	fmt.Println(strings)
}

// this function is a mix
// ONLY THE LAST PARAMETER CAN BE VARIADIC - it can't be placed in front of required parameters
func mix(num int, flag bool, strings ...string) {
	fmt.Println(num, flag, strings)
}

// function takes any number of arguments, returns a float
// returns the highest value from a slice of arguments
func maximum(numbers ...float64) float64 {
	max := math.Inf(-1) // starts with a low value, inf returns positive infinity if sign >=0, negative infinity if sign is <0
	// iterate over the slice of numbers to find the highest value
	for _, number := range numbers {
		if number > max {
			max = number
		}
	}
	return max
}

// function that returns numbers that greater than or equal to the min value or less than the max value
func inRange(min float64, max float64, numbers ...float64) []float64 {
	var result []float64 // slice will hold arguments that are within range
	for _, number := range numbers {
		if number >= min && number < max { // if the condition is true
			result = append(result, number) // add to slice to be returned
		}
	}
	return result
}
