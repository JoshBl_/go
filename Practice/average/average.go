package main

import (
	"datafile"
	"fmt"
	"log"
)

func main() {
	//array literal
	numbers, err := datafile.GetFloats("data.txt")
	if err != nil {
		log.Fatal(err)
	}
	var sum float64 = 0 // this will hold the sum of the three numbers

	for _, number := range numbers {
		sum += number
	}

	sampleCount := float64(len(numbers)) // getting array length and converting it to a float64

	fmt.Printf("Average : %0.2f\n", sum/sampleCount)

}
