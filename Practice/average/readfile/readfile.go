package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	// open the data file for reading
	file, err := os.Open("data.txt")

	// if there was an error, log it and exit
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file) // new scanner for the file
	// reads a line from the file
	for scanner.Scan() {
		fmt.Println(scanner.Text()) // print the line
	}

	// close the file to free resources
	err = file.Close()

	// if there are any errors with closing the file, log it and exit
	if err != nil {
		log.Fatal(err)
	}

	// if there are any errors with the scanner, log it and exit
	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
}
