// quiz game
package main

// math/rand is the import path, the last (or only) segment of the import path is used as the package name
import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	// rand.Intn returns a random integer between 0 and the number provided, 100 in this case
	// storing result in variable called target
	// to get different random numbers, pass a value to rand.Seed (this will "seed" the random number generator - or give it a value that it will use to generate other random values)
	seconds := time.Now().Unix() // this gets the current date and time as an integer
	rand.Seed(seconds)           // seed the random number generator
	target := rand.Intn(100) + 1
	fmt.Println("I've choosen a random number between 1 and 100\nCan you guess it?")

	reader := bufio.NewReader(os.Stdin)

	// boolean declared
	success := false

	// guesses := 3 is the initialisation statement
	// followed by the condition expression
	// then the post statement
	for guesses := 0; guesses < 10; guesses++ {
		fmt.Println("You have", 10-guesses, "guesses left.")

		input, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		input = strings.TrimSpace(input)
		guess, err := strconv.Atoi(input)
		if err != nil {
			log.Fatal(err)
		}

		if guess < target {
			fmt.Println("Oops, your guess was LOW.")
		} else if guess > target {
			fmt.Println("Oops, your guess was HIGH")
		} else {
			fmt.Println("Well done, you guessed the right number!")
			success = true
			break
		}
	}
	if !success {
		fmt.Println("Sorry, you didn't guess the number! It was:", target)
	}
}
