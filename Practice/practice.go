// go practice
package main

// import packages
// remember - any packages that are unused are removed
import (
	"fmt"
	"reflect"
	"strings"
	"time"
)

// new function
func stringReplacer() {
	// functions belong to a package, methods belong to an individual value
	// short hand variable declaration
	broken := "G# r#cks!"
	// NewReplacer function takes two arguments, the string to be replaced and the replaced string
	replacer := strings.NewReplacer("#", "o")
	// fixed variable calls the Replace method
	fixed := replacer.Replace(broken)
	// prints the improved string
	fmt.Println(fixed)
}

func formattingFunc() {
	// using verbs
	fmt.Printf("The %s cost is %d pence each.\n", "Chocolate", 50)
	// %0.2f broken down from the start is;
	/* start of the formatting verb
	minimum width of the entire number
	width after the decimal point
	formatting verb type
	*/
	fmt.Printf("That will be $%0.2f please.\n", 0.50*5)
}

// new function returns a float64 pointer
func pointerFunc() *float64 {
	var newFloat = 75.5
	// return a pointer
	return &newFloat
}

// main function
func main() {
	// new variable called now, uses the Time struct and time package, holds the value of the current time
	var now time.Time = time.Now()
	fmt.Println("Hello user! Enter your name.")
	name := ""
	fmt.Scan(&name)
	fmt.Println("Hello", name, "! By the way, the time is", now)
	// display the day, month and year by calling the now variable
	// now variable holds a time.Time value - we call methods on the time.Time value
	fmt.Println(now.Day(), now.Month(), now.Year())
	stringReplacer()
	formattingFunc()
	fmt.Println("Let's access an address for our variable called name -", name)
	// the value held at the address will be what the user types in
	// the value that represent the address of a variable are known as pointers
	// use &
	fmt.Println(&name)
	// lets use reflect to get a pointer and printer the pointers type
	fmt.Println(reflect.TypeOf(&name))
	// we can declare variables that hold pointers, a pointer variable can only hold pointers to one type of value
	myStringPointer := &name
	fmt.Println(myStringPointer)
	// you can get the value of the variable a pointer refers to by typing the * operator before the pointer
	// also pronounced as 'value at my variable'
	fmt.Println(*myStringPointer)
	// we can also update the value at the pointer
	*myStringPointer = "James"
	// since the value at the address has changed, the original variable changes as well
	fmt.Println(name)
	var floatPointer *float64 = pointerFunc() //assign the returned pointer to a variable
	fmt.Println(*floatPointer)
}
