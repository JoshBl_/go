package main

import (
	"fmt"
)

func main() {
	// defining a new slice - we don't declare a size!
	var mySlice []string //empty pair of brackets and the type of elements the slice can hold

	// create a slice with 7 strings
	mySlice = make([]string, 7)

	mySlice[0] = "New"
	mySlice[1] = "Test"
	mySlice[2] = "Note"
	mySlice[3] = "Hello"
	mySlice[4] = "World"
	mySlice[5] = "Golang"
	mySlice[6] = ".NET"

	fmt.Println(mySlice[0], mySlice[1], mySlice[2])

	// creating a slice with integers and set up a variable to hold it
	primes := make([]int, 5)
	primes[0] = 2
	primes[1] = 3
	fmt.Println(primes[0])

	// len also works with slices!
	fmt.Println(len(mySlice))
	fmt.Println(len(primes))

	// and for loops as well!
	for i := 0; i < len(mySlice); i++ {
		fmt.Println(mySlice[i])
	}

	// slice literal
	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8}
	for i := 0; i < len(numbers); i++ {
		fmt.Println(numbers[i])
	}

	// creating an array and then making a slice based on it using the slice operator
	myArray := [5]string{"a", "b", "c", "d", "e"}
	myNewSlice := myArray[0:3] //we specify the index where the slice should start and the index of array slice should stop before
	// if you omit the stop index - everything will be included in the slice

	for i := 0; i < len(myNewSlice); i++ {
		fmt.Println(myNewSlice[i])
	}
}
