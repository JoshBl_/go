package main

import (
	"fmt"
)

// Litres - float64 type
type Litres float64

// Gallons - float64 type
type Gallons float64

// Millilitres - float64 type
type Millilitres float64

func main() {
	var carFuel Gallons
	var busFuel Litres
	carFuel = Gallons(10.0) // converting a float64 to gallons
	busFuel = Litres(140.0) // converting a float64 to litres
	fmt.Println(carFuel)
	fmt.Println(busFuel)

	// converting to different types
	carFuel = Gallons(Litres(40.0) * 0.264) // convert from Litres to Gallons
	busFuel = Litres(Gallons(63.0) * 3.785) // convert from Gallons to Litres
	fmt.Printf("Gallons: %0.1f Litres: %0.1f\n", carFuel, busFuel)

	// you can use operations with types - but only with the same type (i.e. flaot64 in this case)
	fmt.Println(Litres(1.2) == 1.2)
	fmt.Println(Litres(1.2) + 1.2)

	fmt.Printf("Car fuel: %0.1f gallons\n", carFuel)
	fmt.Printf("Bus fuel: %0.1f litres\n", busFuel)

	// calling the functions to convert values
	carFuel += ToGallons(Litres(40.0))
	busFuel += ToLitres(Gallons(30.0))

	fmt.Println("After addition and conversion")

	fmt.Printf("Car fuel: %0.1f gallons\n", carFuel)
	fmt.Printf("Bus fuel: %0.1f litres\n", busFuel)

	// calling method - carFuel is the Method receiver and displayGallons is the Method name
	carFuel.displayGallons()
	carFuel.newMethod(55.5, true, "Hello world")
	// value is automatically converted into a pointer
	carFuel.doubleMethod() // NOTE - we don't need to update the method call when dealing with pointers!
	fmt.Printf("Car fuel: %0.1f gallons\n", carFuel)

	pepsi := Litres(2)
	fmt.Printf("%0.3f litres equals %0.3f gallons\n", pepsi, pepsi.LitresToGallons())
	sparklingWater := Millilitres(500)
	fmt.Printf("%0.3f millilitres equals %0.3f gallons\n", sparklingWater, sparklingWater.MillilitresToGallons())

	milk := Gallons(2)
	fmt.Printf("%0.3f gallons equals %0.3f litres\n", milk, milk.GallonsToLitres())
	fmt.Printf("%0.3f gallons equals %0.3f millilitres\n", milk, milk.GallonsToMillilitres())

	fmt.Println(Gallons(12.09248342))
	fmt.Println(Litres(12.09248342))
	fmt.Println(Millilitres(12.09248342))

}

// Methods that satisfy Stringer interface

func (g Gallons) String() string {
	return fmt.Sprintf("%0.2f gal", g)
}

func (l Litres) String() string {
	return fmt.Sprintf("%0.2f L", l)
}

func (m Millilitres) String() string {
	return fmt.Sprintf("%0.2f mL", m)
}

// ToGallons - function to convert Litres to Gallons - returns the value in type of Gallons
func ToGallons(l Litres) Gallons {
	return Gallons(l * 0.264)
}

// ToLitres - function to convert Gallons to Litres - returns the value in type of Litres
func ToLitres(g Gallons) Litres {
	return Litres(g * 3.785)
}

// Method - receiver parameters in parentheses before the function name
func (g Gallons) displayGallons() { // g is the receiver parameter name and Gallons is the receiver parameter type
	fmt.Printf("Gallons: %0.1f\n", g)
}

// another method, this time with parameters
func (g Gallons) newMethod(number float64, flag bool, text string) {
	fmt.Println(number)
	fmt.Println(flag)
	fmt.Println(text)
}

// method that doubles current value - uses pointers to return value back to the original variable
func (g *Gallons) doubleMethod() { // use a * on the receiver type to indicate it's a pointer type
	*g *= 2 // update the value at the pointer
}

// LitresToGallons - this method applies to a variable of the Litres type
func (l Litres) LitresToGallons() Gallons {
	return Gallons(l * 0.264)
}

// MillilitresToGallons - this method applies to a variable of the Millilitres type
func (m Millilitres) MillilitresToGallons() Gallons {
	return Gallons(m * 0.000264)
}

// GallonsToLitres - this method applies to a variable of the Gallons type
func (g Gallons) GallonsToLitres() Litres {
	return Litres(g * 3.785)
}

// GallonsToMillilitres - this method applies to a variable of the Gallons type
func (g Gallons) GallonsToMillilitres() Litres {
	return Litres(g * 3785.41)
}
