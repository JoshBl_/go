package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("Maps!")
	// declaring a map - key type in brackets and value type outside of them
	var myMap map[string]float64
	// you need to call the make function
	myMap = make(map[string]float64)

	// adding values to the map
	myMap["Hello world"] = 1.0
	myMap["Go rocks"] = 2.0

	// printing the map
	fmt.Println(myMap)

	// you can use shorthand declaration
	ranks := make(map[string]int) //creates a map and decare a variable to hold it
	ranks["Joshua"] = 1

	// printing the map
	fmt.Println(ranks)

	elements := make(map[string]string)
	elements["H"] = "Hydrogen"
	elements["Li"] = "Lithium"

	// printing a specific element from the map by using the key
	fmt.Println(elements["H"])
	// printing a specific element from the map by using the key
	fmt.Println(elements["Li"])

	// map literals - if you know keys and values that you want your map to have in advance
	// after defining the map type - the curly braces contain the key/value pairs
	mapLiteral := map[int]string{1: "Test", 2: "Hello world", 3: "Go rocks"}

	fmt.Println(mapLiteral[1])
	fmt.Println(mapLiteral[2])
	fmt.Println(mapLiteral[3])

	counters := map[string]int{"a": 3, "b": 0}
	var value int
	var ok bool
	// access an assigned value - ok is true
	value, ok = counters["a"]
	fmt.Println(value, ok)
	value, ok = counters["b"]
	fmt.Println(value, ok)
	// access an unassigned value - ok is false
	value, ok = counters["c"]
	// you can use a blank identifier to test for the values presence, but ignore it
	_, ok = counters["a"]
	fmt.Println(value, ok)

	// removing a value from the map by specifying the key to be removed with it's corresponding value
	delete(counters, "b")
	value, ok = counters["b"]
	fmt.Println(value, ok)

	// PRINTING VALUES FROM A MAP IN ORDER

	// create a new slice of keys from the map
	var numbers []int
	for num := range mapLiteral {
		numbers = append(numbers, num)
	}

	// sort the slice to be from lowest to highest
	sort.Ints(numbers)

	// iterating over the slice and printing the value associated with the key
	for _, number := range numbers {
		fmt.Println(number, "has a value of", mapLiteral[number])
	}
}
