package main

import (
	"fmt"
	"time"
)

func main() {
	// decalre a new array with strings of a length of 7
	var notes [7]string
	notes[0] = "Test"         // assign a value to the first element
	notes[1] = "Another Test" // assign a value to the second element
	notes[2] = "Extra test"   // assign a value to the third element

	fmt.Println(notes[0])
	fmt.Println(notes[1])

	// declare a new array with integers of a length of 5
	var primes [5]int
	primes[0] = 2 // assign a value to the first element
	primes[1] = 3 // assign a value to the second element

	fmt.Println(primes[0])

	// declare a new array with time of a length of 3
	var dates [3]time.Time
	dates[0] = time.Unix(1257894000, 0) // assign a value to the first element
	dates[1] = time.Unix(1447920000, 0) // assign a value to the second element
	dates[2] = time.Unix(1508632200, 0) // assign a value to the third element

	fmt.Println(dates[1])

	// array literal - we know in advance what values an array should hold
	literalTest := [3]int{10, 12, 134}
	fmt.Println(literalTest[0])

	textTest := [5]string{"Hello", "Test", "Text", "New notes", "Go rocks"}
	fmt.Println(textTest[3])

	// format arrays to appear as Go code
	fmt.Printf("%#v\n", textTest)
	fmt.Printf("%#v\n", literalTest)

	// getting the legnth of an array
	fmt.Println("The length of the textTest array is", len(textTest))
	// looping through an array
	for i := 0; i < len(textTest); i++ {
		fmt.Println(i, textTest[i])
	}

	// a safer way of looping - range
	// index = variable that will hold each element's index
	// value = variable that will hold each element's value
	// range keyword with the array
	for index, value := range textTest {
		fmt.Println(index, value)
	}

	// we can use the blank identifier if we want
	for _, value := range literalTest {
		fmt.Println(value)
	}
}
