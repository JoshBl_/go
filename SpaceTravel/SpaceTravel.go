package main

import "fmt"

func fuelGuage(fuel int) {
	fmt.Printf("\nPilot - you have %d fuel left!", fuel)
}

func calculateFuel(planet string) int {
	var fuel int
	switch planet {
	case "Venus":
		fuel = 300000
	case "Mercury":
		fuel = 500000
	case "Mars":
		fuel = 700000
	default:
		fuel = 0
	}
	return fuel
}

func greetPlanet(planet string) {
	fmt.Printf("\nAttention everyone, we are now approaching %v", planet)
}

func cantFly() {
	fmt.Println("\nWe do not have the available fuel to fly there")
}

func flyToPlanet(planet string, fuel int) int {
	var fuelRemaining int
	var fuelCost int

	fuelRemaining = fuel
	fuelCost = calculateFuel(planet)
	if fuelRemaining >= fuelCost {
		greetPlanet(planet)
		fuelRemaining -= fuelCost
	} else {
		cantFly()
	}
	fuelGuage(fuelRemaining)
	return fuelRemaining
}

func main() {
	var fuel int = 1000000
	var planetChoice = "Venus"
	flyToPlanet(planetChoice, fuel)

	planetChoice = "Mars"

	flyToPlanet(planetChoice, fuel)
}
