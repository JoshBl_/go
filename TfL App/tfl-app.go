// TfL app program rewrite (originally written in Python)
package main

// factored import statement (parenthesised)
import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

// variables for app key and ID
var appID = ""
var appKey = ""

// main function
func main() {
	// boolean declared for 'for' loop
	check := true
	// while check is true - loop
	for check == true {
		fmt.Println("Welcome to the TfL app!\nPlease specify which service you would like to check!\n1) Underground status\n2) Road status\n3) Route Checker\nq) Quit")
		var choice string
		fmt.Scan(&choice)
		// switch statemet
		switch choice {
		case "1":
			fmt.Println("Underground status")
			tubeCheck()
		case "2":
			fmt.Println("Road status")
			roadCheck()
		case "3":
			fmt.Println("Route Checker")
			travelRoute()
		case "q":
			fmt.Println("Goodbye!")
			// change value of boolan to end loop and program
			check = false
		default:
			// if user enters anything else
			fmt.Println("Error, please try again!")
		}
	}
}

// tubeCheck function
func tubeCheck() {
	// struct for decoding response into a structure - this response is an array of objects
	// making it a type and capital letter so it can be exported
	type TubeStatuses struct {
		// note about the response from all of the APIs, type will be described as '$type', $ removed
		// inner struct
		LineStatuses []struct {
			StatusSeverity            int    `json:"statusSeverity"`
			StatusSeverityDescription string `json:"statusSeverityDescription"`
		} `json:"lineStatuses"`
		Name string `json:"name"`
	}

	fmt.Println("Now retrieving Underground line status, please wait...")
	// two variables (response and error) which stores the response from GET request
	getRequest, err := http.Get("https://api.tfl.gov.uk/line/mode/tube/status")
	fmt.Println("The status code is", getRequest.StatusCode, http.StatusText(getRequest.StatusCode))

	if err != nil {
		fmt.Print(err)
	}

	//close - this will be done at the end of the function
	// it's important to close the connection - we don't want the connection to leak
	defer getRequest.Body.Close()

	// read the body of the GET request
	rawData, err := ioutil.ReadAll(getRequest.Body)

	// if reading the body of the JSON returns an error, print it
	if err != nil {
		fmt.Print(err)
	}

	// new array variable of TubeStatuses
	ts := []TubeStatuses{}

	// unmarshal the json response - using the address of the ts variable
	jsonErr := json.Unmarshal(rawData, &ts)

	// if unmarshaling the response returns an error - print it
	if jsonErr != nil {
		fmt.Println(jsonErr)
	}

	fmt.Println("Welcome to the TfL Underground checker!\nPlease enter a number for the line you want to check!\n0 - Bakerloo\n1 - central\n2 - circle\n3 - district\n4 - hammersmith & City\n5 - jubilee\n6 - metropolitan\n7 - northern\n8 - piccadilly\n9 - victoria\n10 - waterloo & city")
	// creating a new reader
	reader := bufio.NewReader(os.Stdin)
	// allowing user to add input
	_, _ = reader.ReadString('\n')
	// reading input
	choice, errRead := reader.ReadString('\n')

	// if there are any errors with reading input - print them
	if errRead != nil {
		fmt.Print(errRead)
	}
	// remove any trailing and leading whitespace from the variable
	choice = strings.TrimSpace(choice)

	// convert string to int
	tubeLine, errConvert := strconv.Atoi(choice)

	// if there are any errors in converting - print them
	if errConvert != nil {
		fmt.Print(errConvert)
	}

	// display element from the struct
	fmt.Println("Welcome to the", ts[tubeLine].Name, "line!")
	fmt.Println("The current status on the", ts[tubeLine].Name, "line is:", ts[tubeLine].LineStatuses[0].StatusSeverityDescription)

	// breaking up parts of the program, makes it easier to read
	fmt.Print("\n")
	fmt.Println("Press enter to see the stops with this line.")
	showStops, _ := reader.ReadString('\n')
	fmt.Println(showStops)

	fmt.Println("Now showing all stops for the", ts[tubeLine].Name, "line:")

	// new get request
	getRequestStops, err := http.Get("https://api.tfl.gov.uk/line/" + ts[tubeLine].Name + "/stoppoints")

	// read body of the response
	tubeStopsRaw, err := ioutil.ReadAll(getRequestStops.Body)

	// struct for the stops on the underground
	type TubeStops struct {
		StopType   string `json:"stopType"`
		Status     bool   `json:"status"`
		CommonName string `json:"commonName"`
	}

	// array of struct
	listOfStops := []TubeStops{}

	// unmarshal data and store in the address of variable
	jsonErr = json.Unmarshal(tubeStopsRaw, &listOfStops)

	if jsonErr != nil {
		fmt.Print(jsonErr)
	}

	// iterating over the array of objects in listOfStops
	for _, stopCount := range listOfStops {
		// for every object, do the following
		fmt.Println(stopCount.CommonName)
	}

	fmt.Print("\n")
	fmt.Println("Press enter to return to the main menu.")
	enter, _ := reader.ReadString('\n')
	fmt.Println(enter)

}

// roadCheck function
func roadCheck() {
	// struct to store json response - response is an array of objects
	type RoadStatus struct {
		DisplayName               string `json:"displayName"`
		StatusSeverity            string `json:"statusSeverity"`
		StatusSeverityDescription string `json:"statusSeverityDescription"`
	}
	fmt.Println("Now retrieving London road status, please wait...")
	// array variable of struct
	rs := []RoadStatus{}
	// two variables (response and error) which stores the response from the GET request
	getRequest, err := http.Get("https://api.tfl.gov.uk/road")

	// if there are any errors with the request - print them
	if err != nil {
		fmt.Print(err)
	}

	//close - this will be done at the end of the function
	// it's important to close the connection - we don't want the connection to leak
	defer getRequest.Body.Close()

	// read the body of the GET request
	rawData, err := ioutil.ReadAll(getRequest.Body)

	// if there are any errors with reading input - print them
	if err != nil {
		fmt.Print(err)
	}

	// unmarshal or decode the json, result stored in interface
	json.Unmarshal(rawData, &rs)
	fmt.Println("The status code is", getRequest.StatusCode, http.StatusText(getRequest.StatusCode))
	fmt.Println("Welcome to TfL Road checker!\nType in the number that you would like to check!\n0 - A1\n1 - A10\n2 - A12\n3 - A13\n4 - A2\n5 - A20\n6 - A205\n7 - A21\n8 - A23\n9 - A24\n10 - A3\n11 - A316\n12 - A4\n13 - A40\n14 - A406\n15 - A41\n16 - Bishopsgate Cross Route\n17 - Blackwall Tunnel\n18 - City Route\n19 - Farringdon Cross Route\n20 - Inner Ring\n21 - Southern River Route\n22 - Western Cross Route")

	// new reader
	reader := bufio.NewReader(os.Stdin)
	// allowing user to add input
	_, _ = reader.ReadString('\n')
	// reading input and converting to int
	choice, err := reader.ReadString('\n')

	// if there are any errors with reading input - print them
	if err != nil {
		fmt.Print(err)
	}

	// remove any trailing and leading whitespace from the variable
	choice = strings.TrimSpace(choice)

	// convert string to int
	roadSelected, err := strconv.Atoi(choice)

	// if there are any errors with conversion - print them
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("The current status of", rs[roadSelected].DisplayName, "is", rs[roadSelected].StatusSeverity, "with", rs[roadSelected].StatusSeverityDescription)
	fmt.Println("Press enter to return to the main menu.")
	enter, _ := reader.ReadString('\n')
	fmt.Println(enter)
}

// travelRoute function
func travelRoute() {
	// TRIM THE WHITESPACE FROM THE INPUT VARIABLES OTHERWISE THE GET REQUEST PANICS!
	reader := bufio.NewReader(os.Stdin)
	// for some reason, you need to call the reader so the user can enter what they want for the startingPoint and endPoint variable
	// happens when calling the function from a switch case
	_, _ = reader.ReadString('\n')
	fmt.Println("Please enter the name of the underground station where you are travelling from (i.e. Euston):\nPlease note, travel information will be based on the current time.")
	startingPoint, _ := reader.ReadString('\n')
	startingPoint = strings.TrimSpace(startingPoint)
	fmt.Printf("You have selected %s", startingPoint)
	fmt.Println("\nPlease enter the name of the underground station - end destination (i.e. Westminster):")
	endingPoint, err := reader.ReadString('\n')

	if err != nil {
		fmt.Println(err)
	}

	endingPoint = strings.TrimSpace(endingPoint)
	fmt.Printf("You have selected %s", endingPoint)
	fmt.Println("\nNow fetching route, please wait...")
	getRequest, err := http.Get("https://api.tfl.gov.uk/journey/journeyresults/" + startingPoint + "/to/" + endingPoint + "&app_id=" + appID + "&app_key=" + appKey)

	if err != nil {
		fmt.Println(err)
	}

	rawData, err := ioutil.ReadAll(getRequest.Body)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("The status code is", getRequest.StatusCode, http.StatusText(getRequest.StatusCode))

	//struct for locating icsCodes
	type IcsCodes struct {
		Type                       string `json:"type"`
		FromLocationDisambiguation struct {
			DisambiguationOptions []struct {
				ParameterValue string `json:"parameterValue"`
			} `json:"disambiguationOptions"`
		} `json:"fromLocationDisambiguation"`
		ToLocationDisambiguation struct {
			DisambiguationOptions []struct {
				ParameterValue string `json:"parameterValue"`
			} `json:"disambiguationOptions"`
		} `json:"toLocationDisambiguation"`
	}

	// variable for struct
	icsCode := IcsCodes{}

	jsonErr := json.Unmarshal(rawData, &icsCode)

	if jsonErr != nil {
		fmt.Println(jsonErr)
	}

	fmt.Println("The ICS Code for the starting point is", icsCode.FromLocationDisambiguation.DisambiguationOptions[0].ParameterValue)
	fmt.Println("The ICS Code for the end point is", icsCode.ToLocationDisambiguation.DisambiguationOptions[0].ParameterValue)

	// assigning the values of the ICS Codes to new variables
	var startPoint = icsCode.FromLocationDisambiguation.DisambiguationOptions[0].ParameterValue
	startPoint = strings.TrimSpace(startPoint)

	var endPoint = icsCode.ToLocationDisambiguation.DisambiguationOptions[0].ParameterValue
	endPoint = strings.TrimSpace(endPoint)

	// performing get request to get route
	getJourney, err := http.Get("https://api.tfl.gov.uk/journey/journeyresults/" + startPoint + "/to/" + endPoint + "&app_id=" + appID + "&app_key=" + appKey)
	fmt.Println("The status code is", getJourney.StatusCode, http.StatusText(getJourney.StatusCode))
	journeyData, err := ioutil.ReadAll(getJourney.Body)

	//close - this will be done at the end of the function
	// it's important to close the connection - we don't want the connection to leak
	defer getRequest.Body.Close()

	//struct for completed journey
	type RetrievedJourneys struct {
		Type     string `json:"type"`
		Journeys []struct {
			Duration int `json:"duration"`
			Legs     []struct {
				Duration    int `json:"duration"`
				Instruction struct {
					Type     string `json:"type"`
					Summary  string `json:"summary"`
					Detailed string `json:"detailed"`
					Steps    []struct {
						Description        string `json:"description"`
						TurnDirection      string `json:"turnDirection"`
						StreetName         string `json:"streetName"`
						DescriptionHeading string `json:"descriptionHeading"`
					} `json:"steps"`
				} `json:"instruction"`
				DepartureTime  string `json:"departureTime"`
				ArrivalTime    string `json:"arrivalTime"`
				DeparturePoint struct {
					CommonName string `json:"commonName"`
				} `json:"departurePoint,omitempty"`
				ArrivalPoint struct {
					CommonName string `json:"commonName"`
				} `json:"arrivalPoint"`
				RouteOptions []struct {
					Name       string   `json:"name"`
					Directions []string `json:"directions"`
				} `json:"routeOptions"`
				Disruptions []struct {
					Description string `json:"description"`
				} `json:"disruptions"`
				IsDisrupted bool `json:"isDisrupted"`
			} `json:"legs"`
			Fare struct {
				TotalCost int `json:"totalCost"`
				Fares     []struct {
					Cost              int    `json:"cost"`
					ChargeProfileName string `json:"chargeProfileName"`
					IsHopperFare      bool   `json:"isHopperFare"`
					ChargeLevel       string `json:"chargeLevel"`
					Peak              int    `json:"peak"`
					OffPeak           int    `json:"offPeak"`
				} `json:"fares"`
			} `json:"fare"`
		} `json:"journeys"`
	}

	// variable of struct
	journeyInfo := RetrievedJourneys{}

	// unmarshal the response of the GET request
	jsonError := json.Unmarshal(journeyData, &journeyInfo)

	if jsonError != nil {
		fmt.Println(jsonError)
	}

	// displaying the journey time
	fmt.Println("The journey time will be", journeyInfo.Journeys[1].Duration, "minutes.")

	// showing the total cost of a journey (can be applied to both peak and off peak)
	// find the fare from the struct
	totalCostInt := journeyInfo.Journeys[0].Fare.TotalCost
	// convert value into float64
	totalCost := float64(totalCostInt)
	// divide by 100
	totalCost = totalCost / 100
	// convert back into a string to a precison of 2
	totalCostString := strconv.FormatFloat(totalCost, 'f', 2, 64)
	fmt.Println("The total cost of this journey will be £", totalCostString)
	// showing the peak cost of a journey
	peakCostInt := journeyInfo.Journeys[0].Fare.Fares[0].Peak
	peakCost := float64(peakCostInt)
	peakCost = peakCost / 100
	peakCostString := strconv.FormatFloat(peakCost, 'f', 2, 64)
	fmt.Println("The total cost of this journey on peak times will be £", peakCostString)
	// showing the off peak cost of a journey
	offPeakCostInt := journeyInfo.Journeys[0].Fare.Fares[0].OffPeak
	offPeakCost := float64(offPeakCostInt)
	offPeakCost = offPeakCost / 100
	offPeakCostString := strconv.FormatFloat(offPeakCost, 'f', 2, 64)
	fmt.Println("The total cost of this journey on off peak times will be £", offPeakCostString)
	fmt.Print("\n")

	// assigning the objects in the Legs array to a variable
	journeyLeg := journeyInfo.Journeys[0].Legs

	legCount := 0

	// iterating over the objects in journeyLeg
	for _, journeyCount := range journeyLeg {

		fmt.Println("From", journeyCount.DeparturePoint.CommonName)
		fmt.Println(journeyCount.Instruction.Summary)
		// variable that holds the objects within Steps struct
		journeySteps := journeyCount.Instruction.Steps
		// inner for loop - iterate over the objects in journeySteps
		for _, steps := range journeySteps {
			fmt.Println(steps.DescriptionHeading, steps.Description)
		}
		fmt.Println("Arrive at", journeyCount.ArrivalPoint.CommonName)

		// variable with the value of IsDisrupted field
		isDisrupted := journeyCount.IsDisrupted
		// check the value of the variable isDisrupted
		if isDisrupted == true {
			fmt.Println("=======Disruption:=======")
			fmt.Println(journeyCount.Disruptions[legCount].Description)
		} else {
			fmt.Println("No Disruptions reported")
		}
		fmt.Print("\n")
	}

	fmt.Println("Press enter to return to the main menu.")
	enter, _ := reader.ReadString('\n')
	fmt.Println(enter)
}
