// TfL app program rewrite (originally written in Python)
package main

// import packages
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	// struct for decoding into a structure - this is an array
	// making it a type and capital letter so it can be exported
	// struct to store json response - as an array
	// TRIM THE WHITESPACE FROM THE INPUT VARIABLES OTHERWISE THE GET REQUEST PANICS!
	fmt.Println("\nNow fetching route, please wait...")
	getRequest, err := http.Get("https://api.tfl.gov.uk/journey/journeyresults/Euston/to/CanaryWharf&app_id=028495b7&app_key=bd7089722802f245d2d183c4be394ef8")

	if err != nil {
		fmt.Println(err)
	}

	rawData, err := ioutil.ReadAll(getRequest.Body)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("The status code is", getRequest.StatusCode, http.StatusText(getRequest.StatusCode))

	//struct for locating icsCodes
	type IcsCodes struct {
		Type                       []string `json:"type"`
		FromLocationDisambiguation struct {
			Type                  string `json:"type"`
			DisambiguationOptions []struct {
				Type           string `json:"type"`
				MatchQuality   int    `json:"matchQuality"`
				ParameterValue string `json:"parameterValue"`
				Place          struct {
					Type                 string        `json:"type"`
					AdditionalProperties []interface{} `json:"additionalProperties"`
					CommonName           string        `json:"commonName"`
					IcsCode              string        `json:"icsCode"`
					Lat                  float64       `json:"lat"`
					Lon                  float64       `json:"lon"`
					Modes                []string      `json:"modes"`
					NaptanID             string        `json:"naptanId"`
					PlaceType            string        `json:"placeType"`
					StopType             string        `json:"stopType"`
					URL                  string        `json:"url"`
				} `json:"place"`
				URI string `json:"uri"`
			} `json:"disambiguationOptions"`
			MatchStatus string `json:"matchStatus"`
		} `json:"fromLocationDisambiguation"`
		JourneyVector struct {
			Type string `json:"type"`
			From string `json:"from"`
			To   string `json:"to"`
			URI  string `json:"uri"`
			Via  string `json:"via"`
		} `json:"journeyVector"`
		RecommendedMaxAgeMinutes int `json:"recommendedMaxAgeMinutes"`
		SearchCriteria           struct {
			Type         string `json:"type"`
			DateTime     string `json:"dateTime"`
			DateTimeType string `json:"dateTimeType"`
		} `json:"searchCriteria"`
		ToLocationDisambiguation struct {
			Type                  string `json:"type"`
			DisambiguationOptions []struct {
				Type           string `json:"type"`
				MatchQuality   int    `json:"matchQuality"`
				ParameterValue string `json:"parameterValue"`
				Place          struct {
					Type                 string        `json:"type"`
					AdditionalProperties []interface{} `json:"additionalProperties"`
					CommonName           string        `json:"commonName"`
					IcsCode              string        `json:"icsCode"`
					Lat                  float64       `json:"lat"`
					Lon                  float64       `json:"lon"`
					Modes                []string      `json:"modes"`
					NaptanID             string        `json:"naptanId"`
					PlaceType            string        `json:"placeType"`
					StopType             string        `json:"stopType"`
					URL                  string        `json:"url"`
				} `json:"place"`
				URI string `json:"uri"`
			} `json:"disambiguationOptions"`
			MatchStatus string `json:"matchStatus"`
		} `json:"toLocationDisambiguation"`
		ViaLocationDisambiguation struct {
			Type        string `json:"type"`
			MatchStatus string `json:"matchStatus"`
		} `json:"viaLocationDisambiguation"`
	}

	// array variable for struct
	icsCode := []IcsCodes{}

	jsonErr := json.Unmarshal(rawData, &icsCode)

	if jsonErr != nil {
		fmt.Println(jsonErr)
	}

	fmt.Println("The ICS Code for the starting point is", icsCode[0].FromLocationDisambiguation.DisambiguationOptions[0].ParameterValue)
	fmt.Println("The ICS Code for the end point is", icsCode[0].ToLocationDisambiguation.DisambiguationOptions[0].ParameterValue)
}
