package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

// Pub contains the ID, name and address
// fields capitalised so they can be exported
type Pub struct {
	Name            string
	Address         string
	TelephoneNumber string
}

// an array of the Pub struct
var pubs = []Pub{
	{
		Name:            "Ye Olde Swan",
		Address:         "Newport Rd, Woughton on the Green, Milton Keynes MK6 3BS",
		TelephoneNumber: "01908 679489",
	},
	{
		Name:            "The Wavendon Arms",
		Address:         "2 Newport Rd, Wavendon, Milton Keynes MK17 8LJ",
		TelephoneNumber: "01908 584277",
	},
	{
		Name:            "Cross Keys",
		Address:         "34 Newport Rd, Woolstone, Milton Keynes MK15 0AA",
		TelephoneNumber: "01908 528145",
	},
	{
		Name:            "Brewhouse & Kitchen",
		Address:         "7 Savoy Crescent, 12th Street MK9 3PU",
		TelephoneNumber: "01908 049032",
	},
	{
		Name:            "The Swan Inn",
		Address:         "Broughton Rd, Milton Keynes MK10 9AH",
		TelephoneNumber: "01908 665240",
	},
	{
		Name:            "The Barge",
		Address:         "15 Newport Rd, Woolstone, Milton Keynes MK15 0AE",
		TelephoneNumber: "01908 233841",
	},
	{
		Name:            "The Black Horse",
		Address:         "Wolverton Rd, Great Linford, Milton Keynes MK14 5AJ",
		TelephoneNumber: "01908 398461",
	},
}

func main() {
	fmt.Println("Fetching pubs - please wait...")
	check := true
	for check == true {
		fmt.Println("Welcome to PubPicker (v1.0)! If you're here then it must be Friday!\nAre you ready to pick a random pub?\n(Type Y for 'Yes' and N for 'No')")
		reader := bufio.NewReader(os.Stdin)
		text, err := reader.ReadString('\n')
		text = strings.TrimSpace(text)
		text = strings.ToUpper(text)

		if err != nil {
			fmt.Println(err)
		}

		switch text {
		case "Y":
			fmt.Println("Let's go to the pub!")
			fmt.Println("Now picking a random pub....")
			// pointer variable
			var randNum *int
			// assign value of function to variable
			randNum = numberGenerator()
			if err != nil {
				fmt.Print(err)
			}
			// printing the value at randNum variable
			fmt.Println("You're going to....")
			var pubNum int
			pubNum = *randNum
			fmt.Println(pubs[pubNum].Name)
			fmt.Println(pubs[pubNum].Address)
			fmt.Println(pubs[pubNum].TelephoneNumber)

			fmt.Println("Press enter to go back to the menu")
			enter, _ := reader.ReadString('\n')
			fmt.Println(enter)
		case "N":
			fmt.Println("Bye!")
			check = false
		default:
			fmt.Println("Error! Try again!")
		}
	}
}

// function returns an integer (pointer)
func numberGenerator() *int {
	lessThan10 := false
	var number int
	for lessThan10 == false {
		value := time.Now().Unix()
		rand.Seed(value)
		number = rand.Intn(100)
		if number <= 6 {
			lessThan10 = true
		}
	}
	// return the address of number variable
	return &number
}
