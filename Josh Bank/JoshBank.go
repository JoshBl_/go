// JoshBank Go Program
package main

// import packages
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"strings"
	"time"
)

// new function to iterate through the currency interface
func printRates(currency map[string]interface{}) {
	fmt.Println("These are currency rates at", time.Now())
	for key, value := range currency {
		fmt.Println(key, value)
	}
	fmt.Println("Enter any key to go back to the menu")
	var enter string
	fmt.Scan(&enter)
}

func swapCurrency(currency map[string]interface{}, value string) {
	fmt.Println("Please enter the three character code you wish to swap currency!")
	var selection string
	fmt.Scan(&selection)
	selection = strings.ToUpper(selection)
	fmt.Printf("You have selected %v\n", selection)
	fmt.Print("Currently, 1", value, " is equal to ", currency[selection], " ", selection, "\n")
	fmt.Println("Enter any key to go back to the menu")
	var enter string
	fmt.Scan(&enter)
}

func printToFile(currency map[string]interface{}) {
	fmt.Println("Now printing!")
	// create file
	// file, err := os.Create("Currency Rates.csv")
	// if err != nil {
	// 	fmt.Println("Error! Cannot create file!")
	// 	log.Fatal(err)
	// }
	// defer file.Close()

	// // write to file
	// writer := csv.NewWriter(file)
	// // used to Flush any buffered data
	// defer writer.Flush()

	// for _, value := range currency {
	// 	err := writer.Write(value)
	// 	checkError("Cannot write to file", err)
	// }

	// fmt.Println("Success!")
	// fmt.Println("Enter any key to go back to the menu")
	// var enter string
	// fmt.Scan(&enter)
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

// main function
func main() {
	fmt.Println("Welcome to Josh Bank! Enter the three character code for your preferred currency!")
	// new variable
	var value string
	// empty interface for Unmarshal
	var result map[string]interface{}
	// read input from user and assign to value string
	fmt.Scan(&value)
	fmt.Println("The type of the variable, value, is", reflect.TypeOf(value))
	//take value and convert to upper case
	value = strings.ToUpper(value)
	// using verb to print what the user selected
	fmt.Printf("You have selected - %v\n", value)
	// two variables (response and error) which stores the response from the GET request
	response, err := http.Get("https://api.exchangeratesapi.io/latest?base=" + value)
	// read the body of the GET request
	rates, err := ioutil.ReadAll(response.Body)
	fmt.Println("The type of the variable response is", reflect.TypeOf(response))
	fmt.Println("The type of the variable err is", reflect.TypeOf(err))
	fmt.Println("The type of the variable rates is", reflect.TypeOf(rates))
	// Unmarshal or Decode the JSON, result stored in interface
	json.Unmarshal([]byte(rates), &result)
	fmt.Println("The type of the variable result is", reflect.TypeOf(result))
	// creating a new interface which is looking at an embedded object
	currency := result["rates"].(map[string]interface{})
	fmt.Println("The type of the variable currency is", reflect.TypeOf(currency))
	// close the response body once finished with it
	response.Body.Close()
	// if the GET request returns an error, print it
	if err != nil || currency == nil {
		fmt.Println("Error! Please try again!")
		fmt.Println(err)
	} else {
		fmt.Println("Success!")
	}

	var exit bool
	exit = true

	// for is Go's equivalent of a while loop
	for exit == true {
		fmt.Println("\nWelcome! What would you like to do? Type the first letter of a menu option!")
		fmt.Println("You're selected currency is", value)
		fmt.Println("(V)iew currency rates")
		fmt.Println("(S)wap currency")
		fmt.Println("(P)rint to file")
		fmt.Println("(Q)uit")

		var choice string
		fmt.Scan(&choice)
		choice = strings.ToUpper(choice)
		switch choice {
		case "V":
			fmt.Println("View currency rates.")
			printRates(currency)
		case "S":
			fmt.Println("Swap currency")
			swapCurrency(currency, value)
		case "P":
			fmt.Println("Print to file")
			printToFile(currency)
		case "Q":
			fmt.Println("Exiting Application")
			// setting to false so for loop can end
			exit = false
		default:
			fmt.Println("Unknown command - please try again!")
		}
	}
}
